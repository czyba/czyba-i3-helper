use super::i3_commands::{CommandError, get_all_workspaces};
use super::i3_messages::Workspace;
use std::collections::HashMap;
use std::str::FromStr;
use std::cmp::max;

pub mod focus_next_screen;
pub mod switch_to_workspace;
pub mod move_container_to_workspace;
pub mod move_container_to_next_screen;

pub enum SubCommandError {
    MultipleActiveScreensOnSameMonitor(i32, String, String),
    IncompatibleWorkspaceName(String),
    NoFocusedScreen,
    CommandError(CommandError),
    CouldNotFindNextScreen,
    WorkspaceOutOfRange(i32),
}

pub trait SubCommand {
    fn execute(&self) -> Result<(), SubCommandError>;
}

pub enum Direction {
    PREVIOUS,
    NEXT
}

fn increment_modulo(old: i32, inc: i32, modulo: i32) -> i32 {
    let new_before_mod = old + inc;
    if new_before_mod >= modulo {
        return new_before_mod % modulo;
    }
    if new_before_mod < 0 {
        return new_before_mod + modulo;
    }
    return new_before_mod;
}

struct ActiveWorkspaceMetaInfo {
    index_to_workspace_map: HashMap<i32, Workspace>,
    focused_screen_index: i32,
    number_of_screens: i32
}

fn find_active_workspace_info_from_workspace_list(workspaces: Vec<Workspace>) -> Result<ActiveWorkspaceMetaInfo, SubCommandError> {
    let mut visible_workspaces = HashMap::new();
    let mut focused_screen = None;
    let mut max_screen = None;
    for workspace in workspaces {
        if !workspace.visible {
            continue;
        }
        let name_parts: Vec<&str> = workspace.name.split(".").collect();
        if name_parts.len() != 2  {
            return Err(SubCommandError::IncompatibleWorkspaceName(workspace.name));
        }
        // part 0 == screen, part 1 == workspace on screen
        let screen_value = match i32::from_str(&name_parts[0]) {
            Ok(v) => v,
            Err(_) => return Err(SubCommandError::IncompatibleWorkspaceName(workspace.name))
        };
        if workspace.focused {
            focused_screen = Some(screen_value);
        }
        match max_screen {
            Some(old_max) => max_screen = Some(max(old_max, screen_value)),
            None => max_screen = Some(screen_value)
        }
        let old = visible_workspaces.insert(screen_value, workspace);
        if let Some(old_workspace) = old {
            // Something went wrong. there were at least 2 WS on the same screen focused
            // unwrap here is safe, since we just inserted it
            let workspace = visible_workspaces.remove(&screen_value).unwrap();
            return Err(SubCommandError::MultipleActiveScreensOnSameMonitor(screen_value, old_workspace.name, workspace.name));
        }
    }

    let focused_screen = match focused_screen {
        Some(v) => v,
        None => return Err(SubCommandError::NoFocusedScreen)
    };

    // Safe, because max screen must be at least focused screen!
    let number_of_screens = max_screen.unwrap() + 1;

    return Ok(ActiveWorkspaceMetaInfo {
        index_to_workspace_map: visible_workspaces,
        focused_screen_index: focused_screen,
        number_of_screens
    });
}

fn execute_on_for_workspace_on_next_screen(
    mode: &Direction,
    callback_with_target_workspace: impl Fn(&Workspace) -> Result<(), CommandError>
) -> Result<(), SubCommandError> {
    let all_workspaces = match get_all_workspaces() {
        Ok(v) => v,
        Err(e) => return Err(SubCommandError::CommandError(e))
    };

    let active_workspace_info = find_active_workspace_info_from_workspace_list(all_workspaces)?;

    let increment = match mode {
        Direction::PREVIOUS => -1,
        Direction::NEXT => 1
    };
    let focused_screen_index = active_workspace_info.focused_screen_index;
    let number_of_screens = active_workspace_info.number_of_screens;

    let mut try_screen_index = increment_modulo(focused_screen_index, increment, number_of_screens);
    while try_screen_index != focused_screen_index {
        let next_workspace = match active_workspace_info.index_to_workspace_map.get(&try_screen_index) {
            Some(workspace) => workspace,
            None => {
                try_screen_index = increment_modulo(try_screen_index, increment, number_of_screens);
                continue
            }
        };
        match callback_with_target_workspace(&next_workspace) {
            Ok(_) => return Ok(()),
            Err(e) => return Err(SubCommandError::CommandError(e))
        }
    }
    return Err(SubCommandError::CouldNotFindNextScreen);
}
