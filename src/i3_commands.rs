use std::process::Command;
use serde_json;

// use super::i3_messages::Output;
use super::i3_messages::Workspace;
use super::i3_messages::I3Result;

#[derive(Debug)]
pub enum CommandError {
    IoError(std::io::Error),
    SerdeError(serde_json::error::Error),
    ReturnValueError(i32),
    I3CommandError,
}

trait I3Command {
    fn get_command_type() -> &'static str;
}

macro_rules! define_i3_command {
    ($command_name: ident, $command_string: expr) => (
        struct $command_name {}
        impl I3Command for $command_name {
            fn get_command_type() -> &'static str {
                return $command_string;
            }
        }
    )
}

// define_i3_command!(GetOutputCommand, "get_outputs");
define_i3_command!(GetWorkspaceCommand, "get_workspaces");

fn get_i3_command<R, CommandType: I3Command> (_command: CommandType) -> Result<R, CommandError>
    where R : serde::de::DeserializeOwned
{
    let get_element_result = match Command::new("i3-msg")
            .arg("-t")
            .arg(CommandType::get_command_type())
            .output() {
        Ok(v) => v,
        Err(e) =>  return Err(CommandError::IoError(e))
    };

    if !get_element_result.status.success() {
        return Err(CommandError::ReturnValueError(get_element_result.status.code().unwrap_or(-1i32)));
    }

    let element: R = match serde_json::from_slice(&get_element_result.stdout) {
        Ok(v) => v,
        Err(e) => return Err(CommandError::SerdeError(e))
    };
    return Ok(element);
}

// pub fn get_active_outputs() -> Result<Vec<Output>, CommandError> {
//     let mut result: Vec<Output> = get_i3_command(GetOutputCommand{})?;
//     result.retain(|output| output.active);
//     return Ok(result);
// }

pub fn get_all_workspaces() -> Result<Vec<Workspace>, CommandError> {
    let element: Vec<Workspace> = get_i3_command(GetWorkspaceCommand{})?;
    return Ok(element);
}

fn i3_command(args: &[&str]) -> Result<(), CommandError> {
    let result = match Command::new("i3-msg")
        .args(args)
        .output() {
        Ok(v) => v,
        Err(e) =>  return Err(CommandError::IoError(e))
    };
    if !result.status.success() {
        return Err(CommandError::ReturnValueError(result.status.code().unwrap_or(-1i32)));
    }

    let result: Vec<I3Result> = match serde_json::from_slice(&result.stdout) {
        Ok(v) => v,
        Err(e) => return Err(CommandError::SerdeError(e))
    };

    if !result[0].success {
        return Err(CommandError::I3CommandError)
    }

    return Ok(());
}

pub fn switch_to_workspace(ws_name: &str) -> Result<(), CommandError> {
    return i3_command(&["workspace", ws_name]);
}

pub fn move_container_to_workspace(ws_name: &str) -> Result<(), CommandError> {
    return i3_command(&["move", "container", "to", "workspace", ws_name]);
}

pub fn move_container_to_workspace_and_follow(workspace: &Workspace) -> Result<(), CommandError> {
    let chained_command = format!("move container to output {}; workspace {}", workspace.output, workspace.name);
    return i3_command(&[&chained_command]);
}
