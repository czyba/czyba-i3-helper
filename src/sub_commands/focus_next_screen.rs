use super::{SubCommand, SubCommandError, Direction, execute_on_for_workspace_on_next_screen};
use super::super::i3_commands::switch_to_workspace;

pub struct SwitchToWorkspaceCommand {
    pub direction: Direction
}
impl SubCommand for SwitchToWorkspaceCommand {
    fn execute(&self) -> Result<(), SubCommandError> {
        return execute_on_for_workspace_on_next_screen(
            &self.direction,
            |workspace| switch_to_workspace(&workspace.name)
        );
    }
}
