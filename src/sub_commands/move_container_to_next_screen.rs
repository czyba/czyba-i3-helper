use super::{SubCommand, SubCommandError, Direction, execute_on_for_workspace_on_next_screen};
use super::super::i3_commands::move_container_to_workspace_and_follow;

pub struct MoveContainerToScreen {
    pub direction: Direction
}
impl SubCommand for MoveContainerToScreen {
    fn execute(&self) -> Result<(), SubCommandError> {
        return execute_on_for_workspace_on_next_screen(
            &self.direction,
            |workspace| move_container_to_workspace_and_follow(&workspace)
        );
    }
}
