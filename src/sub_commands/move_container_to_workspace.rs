use super::super::i3_commands::{get_all_workspaces, move_container_to_workspace};
use super::{SubCommand, SubCommandError};

pub struct MoveContainerToWorkspace {
    number: i32
}

impl MoveContainerToWorkspace {
    pub fn new(number: i32) -> Result<MoveContainerToWorkspace, SubCommandError> {
        if number < 0 || number > 9 {
            return Err(SubCommandError::WorkspaceOutOfRange(number));
        }
        return Ok(MoveContainerToWorkspace{ number });
    }
}
impl SubCommand for MoveContainerToWorkspace {
    fn execute(&self) -> Result<(), SubCommandError> {
        return change_to_workspace(self.number);
    }
}

fn change_to_workspace(number: i32) -> Result<(), SubCommandError> {
    let all_workspaces = match get_all_workspaces() {
        Ok(v) => v,
        Err(e) => return Err(SubCommandError::CommandError(e))
    };
    for workspace in all_workspaces {
        if !workspace.focused {
            continue;
        }
        let name_parts: Vec<&str> = workspace.name.split(".").collect();
        if name_parts.len() != 2  {
            return Err(SubCommandError::IncompatibleWorkspaceName(workspace.name));
        }
        let new_workspace_name = format!("{}.{}", name_parts[0], number);
        match move_container_to_workspace(&new_workspace_name) {
            Ok(_) => return Ok(()),
            Err(e) => return Err(SubCommandError::CommandError(e))
        }
    }
    return Err(SubCommandError::NoFocusedScreen)
}
