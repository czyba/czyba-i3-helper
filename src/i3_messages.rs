#[derive(Serialize, Deserialize, Debug)]
pub struct Rect {
    pub x: i32,
    pub y: i32,
    pub width: i32,
    pub height: i32
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Output {
    pub name: String,
    pub active: bool,
    pub primary: bool,
    pub current_workspace: Option<String>,
    pub rect: Rect
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Workspace {
    pub num: i32,
    pub name: String,
    pub visible: bool,
    pub focused: bool,
    pub rect: Rect,
    pub output: String,
    pub urgent: bool
}

#[derive(Serialize, Deserialize, Debug)]
pub struct I3Result {
    pub success: bool
}
