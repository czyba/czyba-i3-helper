use clap::App;
use clap::Error;
use super::sub_commands::{SubCommand, SubCommandError, Direction};
use super::sub_commands::focus_next_screen::SwitchToWorkspaceCommand;
use super::sub_commands::switch_to_workspace::SwitchToWorkspace;
use super::sub_commands::move_container_to_workspace::MoveContainerToWorkspace;
use super::sub_commands::move_container_to_next_screen::MoveContainerToScreen;

pub enum ArgumentError {
    SubCommandCreationError(SubCommandError),
    NoSubcommandChosenError,
    ArgumentParseError(Error),
}

pub fn parse_args() -> Result<Box<SubCommand>, ArgumentError>{
    let yaml = load_yaml!("args.yml");
    let matches = App::from_yaml(yaml).get_matches();
    if let Some(_) = matches.subcommand_matches("focus-next-screen") {
        return Ok(Box::new(SwitchToWorkspaceCommand{ direction: Direction::NEXT }));
    }
    if let Some(_) = matches.subcommand_matches("focus-prev-screen") {
        return Ok(Box::new(SwitchToWorkspaceCommand{ direction: Direction::PREVIOUS}));
    }
    if let Some(_) = matches.subcommand_matches("move-container-to-next-screen") {
        return Ok(Box::new(MoveContainerToScreen{ direction: Direction::NEXT }));
    }
    if let Some(_) = matches.subcommand_matches("move-container-to-prev-screen") {
        return Ok(Box::new(MoveContainerToScreen{ direction: Direction::PREVIOUS }));
    }
    if let Some(submatches) = matches.subcommand_matches("switch-workspace") {
        let workspace = match value_t!(submatches.value_of("workspace"), i32) {
            Ok(v) => v,
            Err(e) => return Err(ArgumentError::ArgumentParseError(e)),
        };
        match SwitchToWorkspace::new(workspace) {
            Ok(change_workspace_command) => return Ok(Box::new(change_workspace_command)),
            Err(e) => return Err(ArgumentError::SubCommandCreationError(e))
        }
    }
    if let Some(submatches) = matches.subcommand_matches("move-container-to-workspace") {
        let workspace = match value_t!(submatches.value_of("workspace"), i32) {
            Ok(v) => v,
            Err(e) => return Err(ArgumentError::ArgumentParseError(e)),
        };
        match MoveContainerToWorkspace::new(workspace) {
            Ok(change_workspace_command) => return Ok(Box::new(change_workspace_command)),
            Err(e) => return Err(ArgumentError::SubCommandCreationError(e))
        }
    }
    return Err(ArgumentError::NoSubcommandChosenError);
}
