extern crate serde_json;
extern crate serde;
#[macro_use] extern crate serde_derive;
#[macro_use] extern crate clap;

mod args;
mod i3_messages;
mod i3_commands;
mod sub_commands;

use args::parse_args;
use std::process::exit;

fn main() {
    // TODO: Serialize to execute only a single command
    let sub_command = match parse_args() {
        Ok(v) => v,
        Err(_) => exit(0xFF) // TODO: print error
    };
    match sub_command.execute() {
        Ok(_) => return,
        Err(_) => exit(0xFF) // TODO: print error
    }
}
